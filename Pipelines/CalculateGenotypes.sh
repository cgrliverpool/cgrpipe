#!/bin/bash

#Get script path and directory
SCRIPT=$(readlink -e $0)
SCRIPTDIR=$(dirname $SCRIPT)
UTILS=$SCRIPTDIR/../Utils/
MODULES=$SCRIPTDIR/../Modules/
AUXSCRIPTS=$SCRIPTDIR/../Scripts/
#Source other files
source $UTILS/Logging
source $UTILS/Error

usage()
{
    cat <<EOF
    usage: $0 options

    OPTIONS:
       -h          Show this message
       -t <INT>    Threads [1]
       -a <FILE>   FQ1
       -b <FILE>   FQ2
       -r <FILE>   Reference fasta (BWA indexed)   
       -o <STRING> Output Prefix
EOF
}

#Variables
THREADS=1
FQ1=
FQ2=
REF=
OUTPREFIX=
LOGFILE=$OUTPREFIX.CalculateGenotypes.log

#Get command line options
while getopts “ht:a:b:r:o:” OPTION
do
    case $OPTION in

        #help
        h)      
            usage
            exit 1
            ;;
	#threads
	t)
	    THREADS=$OPTARG
	    ;;
	#FQ1
	a)
	    FQ1=$OPTARG
	    ;;
	#FQ2
	b)
	    FQ2=$OPTARG
	    ;;
	#Ref
	r)
	    REF=$OPTARG
	    ;;
	#output prefix
	o)
	    OUTPREFIX=$OPTARG
	    ;;
   esac
done

#Check variables
if [ -z $FQ1 ]; then
    echo "Error: FQ1 must be specified with -a"
    usage; exit
fi
if [ -z $FQ2 ]; then
    echo "Error: FQ2 must be specified with -b"
    usage; exit
fi
if [ -z $REF ]; then
    echo "Error: Reference FASTA must be specified with -r"
    usage; exit
fi
if [ -z $OUTPREFIX ]; then
    echo "Error: Output prefix must be specified with -o"
    usage; exit
fi



#Map, filter and sort
$MODULES/BWAMEM_Map -a $FQ1 -b $FQ2 -r $REF -s NA -g NA -t $THREADS -o $OUTPREFIX.srt.bam -e $LOGFILE > $OUTPREFIX.Map.log  2> $OUTPREFIX.Map.err

#Remove duplicates
$MODULES/RmDup -i $OUTPREFIX.srt.bam -o $OUTPREFIX.srt.rmdup.bam -e $LOGFILE > $OUTPREFIX.Rmdup.log 2> $OUTPREFIX.Rmdup.err

#Call variants
$MODULES/CallVariants -i $OUTPREFIX.srt.rmdup.bam -r $REF -o $OUTPREFIX.raw.vcf -p 10 -t $THREADS -e $LOGFILE > $OUTPREFIX.CallVariants.log 2> $OUTPREFIX.CallVariants.err

#Get genotype proportions
$AUXSCRIPTS/GetPloidyHistFromVcf -i $OUTPREFIX.raw.vcf -o $OUTPREFIX.hist.csv

#Plot
$AUXSCRIPTS/genotypeHist.r $OUTPREFIX.hist.csv $OUTPREFIX.geno.pdf





