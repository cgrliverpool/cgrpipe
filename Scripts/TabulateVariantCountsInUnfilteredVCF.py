#!/usr/bin/env python
from optparse import OptionParser


parser = OptionParser()
parser.add_option("-i", "--input", dest="inputFile",  help="input VCF. Must have been annotated with SnpSift vartype and only counts variants with PASS in filter column", metavar="FILE")

(opts, args) = parser.parse_args()

vars = []

sampleNames = []
for line in open(opts.inputFile):

    #Grab the number of samples and set up data structures
    if line.startswith("#CHROM"):
        sampleNames=line.strip().split("\t")[9:]
        for i in range(0,len(sampleNames)):
            vars.append({})
            vars[i]["SNP"]=[0,0] #first=hetCount, second=homCount
            vars[i]["INS"]=[0,0]
            vars[i]["DEL"]=[0,0]

    #Process all variant lines
    elif not line.startswith("#"):
        info = line.strip().split("\t")[7]
        SNP= "SNP" in info.split(";") 
        INS= "INS" in info.split(";")
        DEL= "DEL" in info.split(";")

        #Grab genotypes for each sample
        genoFields = [x.split(":")[0] for x in line.strip().split("\t")[9:]]

        target = ""
        if SNP:
            target ="SNP"
        elif INS:
            target = "INS"
        elif DEL:
            target = "DEL"

        
        
        for i,g in enumerate(genoFields):
            #Ignore reference and non-sequenced variants
            if g=="./." or g=="0/0":
                pass
            else:
                het=g.split("/")[0]!=g.split("/")[1]
                if het:
                    vars[i][target][0]+=1
                else:
                    vars[i][target][1]+=1


print "Sample\tHetSNPs\tHomSNPs\tHetINS\tHomINS\tHetDEL\tHomDEL"
for i,s in enumerate(sampleNames):
    l=[ s,vars[i]["SNP"][0], vars[i]["SNP"][1], vars[i]["INS"][0], vars[i]["INS"][1], vars[i]["DEL"][0], vars[i]["DEL"][1]]
    print '\t'.join([str(x) for x in l])


        
