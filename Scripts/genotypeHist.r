#!/usr/bin/env /pub19/sam/bin/Rscript

#Grab arguments
args <- commandArgs(trailingOnly = TRUE)
inFileName <- args[1]
outFileName <- args[2]
d<-read.csv(file=inFileName,sep=",",head=TRUE)

pdf(outFileName)
hist(d$Data)
dev.off()

