#!/usr/bin/env python
from optparse import OptionParser
import glob

parser = OptionParser()
parser.add_option("-i", "--input", dest="inputDir",  help="Directory containing subfolders (labelled core_chunk_X where X is an integer) each containing a chunk of LS-BSR extract_core_genome output", metavar="DIR")
parser.add_option("-o", "--output", dest="outputFile", help="Output FASTA filename", metavar="FILE")

(opts, args) = parser.parse_args()


seqs={}

dirs =glob.glob("%s/core_chunk_*"%inputDir)

#For each directory, grab the core chunks and store them in the correct dictionary entry in 'seqs'
for dir in dirs:
    NR=0
    for line in open("%s/final_alignment.fasta"%dir):
        
        #Grab Header
        if NR%2==0:
            head=line.strip()[1:]
        #Grab Sequence
        else:
            seq=line.strip()
            seqs[head]=seqs.get(head,"")+seq
        NR+=1

#output
o = open(opts.outputFile,"w")
for i in seqs:
    o.write(">%s\n%s\n"%(i, seqs[i]))
o.close()



