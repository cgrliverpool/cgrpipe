#!/usr/bin/env python
from optparse import OptionParser


parser = OptionParser()
parser.add_option("-i", "--input", dest="inputFile",  help="input VCF. Must have been annotated with SnpEFF and only counts variants with PASS in filter column", metavar="FILE")

(opts, args) = parser.parse_args()

effs = []
impacts = []
effTypes = set()
impactTypes = set()
sampleNames = []
for line in open(opts.inputFile):

    #Grab the number of samples and set up data structures
    if line.startswith("#CHROM"):
        sampleNames=line.strip().split("\t")[9:]
        for i in range(0,len(sampleNames)):
            effs.append({})
            impacts.append({})

    #Process all variant lines
    elif not line.startswith("#") and line.strip().split("\t")[6]=="PASS":
        info = line.strip().split("\t")[7]
        eff = [x.split("=")[1] for x in info.split(";") if "SNPEFF_EFFECT" in x][0]
        impact = [x.split("=")[1] for x in info.split(";") if "SNPEFF_IMPACT" in x][0]
        
        effTypes.add(eff)
        impactTypes.add(impact)

        genoFields = [x.split(":")[0] for x in line.strip().split("\t")[9:]]

        
        
        for i,g in enumerate(genoFields):
            #Ignore reference and non-sequenced variants
            if g=="./." or g=="0/0":
                pass
            else:
                effs[i][eff]=effs[i].get(eff,0)+1
                impacts[i][impact]=impacts[i].get(impact,0)+1

                

effTypeList = list(effTypes)
impactTypeList = list(impactTypes)

h=["Sample"]
for e in effTypeList:
    h.append(e)
print '\t'.join(h)

for i,s in enumerate(sampleNames):
    l=[s]
    for e in effTypeList:
        l.append(effs[i].get(e,0))
    print '\t'.join([str(x) for x in l])

print

h=["Sample"]
for m in impactTypeList:
    h.append(m)
print '\t'.join(h)
for i,s in enumerate(sampleNames):
    l=[s]
    for m in impactTypeList:
        l.append(impacts[i].get(m,0))
    print '\t'.join([str(x) for x in l])
#    l=[ s,vars[i]["SNP"][0], vars[i]["SNP"][1], vars[i]["INS"][0], vars[i]["INS"][1], vars[i]["DEL"][0], vars[i]["DEL"][1]]
#    print '\t'.join([str(x) for x in l])


        
