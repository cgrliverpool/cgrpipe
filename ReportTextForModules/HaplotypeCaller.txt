VERSION="3.3-0-g37228af"
SNP, insertion and deletion discovery was performed with the HaplotypeCaller module of GATK version $VERSION (DePristo et al, 2011). 

#R# DePristo M, Banks E, Poplin R, Garimella K, Maguire J, Hartl C, Philippakis A, del Angel G, Rivas MA, Hanna M, McKenna A, Fennell
 T, Kernytsky A, Sivachenko A, Cibulskis K, Gabriel S, Altshuler D, Daly M (2011). A framework for variation discovery and genotyping using next-generation DNA sequencing data, NATURE GENETICS 43:491-498

