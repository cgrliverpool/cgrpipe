#!/bin/bash

#Get script path and directory
SCRIPT=$(readlink -e $0)
SCRIPTDIR=$(dirname $SCRIPT)
UTILS=$SCRIPTDIR/../Utils/

#MAKERDIR=/usr/local/share/maker-2.11-beta/bin/
MAKERDIR=/usr/local/share/maker-2.31.8/bin/
MAKER=$MAKERDIR/maker
FATHOM=/usr/local/bin/fathom
FORGE=/usr/local/bin/forge
HMMASSEMBLER=/usr/local/bin/hmm-assembler.pl

CTLDIR=/pub28/sam/Databases/Maker/
UNIPROT=/pub28/sam/Databases/Protein/uniprot_sprot.fasta
#Source other files
source $UTILS/Logging
source $UTILS/Error

#ADDED PROTEIN2GENOME

usage()
{
    cat <<EOF
    usage: $0 options

    OPTIONS:
     REQUIRED:
       -g <FILE>   FASTA containing genome/contigs/scaffolds 
       -o <FILE>   Output directory
       -r <FILE>   EST/transcriptome data file from this species (fasta)
       -a <FILE>   EST/transcriptome data file from related species (fasta)
     OPTIONAL:
       -h          Show this message
       -u <FILE>   Use Uniprot proteins, too? Assumed true if -p not used.
       -p <FILE>   Protein data file from (related) species
       -i <INT>    SNAP iterations [3]
       -t <INT>    Threads [1]
       -e <FILE>   Log file [/dev/null]
       
EOF
}

#Create a timestamp
START=$(timestamp)

#Variables
SNAPIT=3
GENOMEFA=
ESTFA=
ALTESTFA=
PROTFA=
OUTDIR=
THREADS=1
USEUNIPROT=0
LOGFILE=/dev/null

#Get command line options
while getopts “hg:o:r:p:a:t:i:ue:” OPTION
do
    case $OPTION in

        #help
        h)      
            usage
            exit 1
            ;;
        #genome FASTA
        g)
            GENOMEFA=$OPTARG
            ;;	
	#output dir
        o)
	    OUTDIR=$OPTARG
	    ;;
        #RNA/EST fasta
	r)
	    ESTFA=$OPTARG
	    ;;
        #RNA/EST fasta from alt species
	a)
	    ALTESTFA=$OPTARG
	    ;;
	#protein fasta
	p)
	    PROTFA=$OPTARG
	    ;;
	#threads
	t)
	    THREADS=$OPTARG
	    ;;
	#use uniprot?
	u)
	    USEUNIPROT=1
	    ;;
	#SNAP iterations
	i)
	    SNAPIT=$OPTARG
	    ;;
	#log file
	e)
	    LOGFILE=$OPTARG
	    ;;
    esac
done

#Verify arguments
if [ -z  $GENOMEFA ]; then 
    echo "Error: Must provide input file with -g"
    usage; exit
fi
if [[ -z  $ESTFA && -z $ALTESTFA ]]; then
    echo "Error: Must provide EST/transcript file with -r or -a"
    usage; exit
fi
if [ -z  $OUTDIR ]; then
    echo "Error: Must provide output dir with -o"
    usage; exit
fi

if [ -z $PROTFA ]; then
    USEUNIPROT=1;
fi

#Append uniprot fa to protfa if required
if [ -z $PROTFA ]; then
    PROTFA=$UNIPROT
else
    if [ $USEUNIPROT -eq 1 ]; then
	PROTFA="$PROTFA,$UNIPROT"
    fi
fi
#Capture stdout and stderr output to temp log file
TMP=$( mktemp -p ./ )
exec >  >(tee -a $TMP >&1)
exec 2> >(tee -a $TMP >&2)

#Introduce script
log "*****"
log "Commencing $(basename $0)"
ICMD="$@"
log "Invoking command: $0 $ICMD" INV

#Command(s)

# 0) Create output directory and cd to it
if [ ! -d $OUTDIR ];then
    CMD="mkdir $OUTDIR"
    log "$CMD" CMD
    eval $CMD
fi

#change dir
#CMD="cd $OUTDIR"
#log "$CMD" CMD
#eval $CMD

# 1) Generate CTL files
CMD="cp $CTLDIR/* $OUTDIR; cp $OUTDIR/maker_opts.ctl $OUTDIR/maker_opts.ctl.bak" 
#CMD="$MAKER -CTL; cp maker_opts.ctl maker_opts.ctl.bak"
log "$CMD" CMD
eval $CMD

# 2) Change CTL file for first run
CMD="cat $OUTDIR/maker_opts.ctl.bak | sed s:^est=:est=$ESTFA:g | sed s:^altest=:altest=$ALTESTFA:g | sed s:^genome=:genome=$GENOMEFA:g | sed s:^protein=:protein=$PROTFA:g | sed s:^est2genome=0:est2genome=1:g | sed s:^protein2genome=0:protein2genome=1:g> $OUTDIR/maker_opts.ctl; cp $OUTDIR/maker_opts.ctl $OUTDIR/maker_opts.ctl.init"
log "$CMD" CMD
eval $CMD

# 3) Run maker
CMD="cd $OUTDIR; mpiexec -n $THREADS $MAKER; cd -" 
log "$CMD" CMD
eval $CMD

# 4) Create HMM and rerun Maker, once for each iteration chosen by user, to refine results


for it in $(seq 1 $SNAPIT);
do
    # 4.1) Grab and merge gffs
    log "4.1) Grab and merge gffs"
    FANOPATH=$(basename $GENOMEFA)
    FANOSUFFIX=${FANOPATH%.*}
    MAKERLOG=$OUTDIR/$FANOSUFFIX.maker.output/${FANOSUFFIX}_master_datastore_index.log
    CMD41="cd $OUTDIR; $MAKERDIR/gff3_merge -d $MAKERLOG; cp $FANOSUFFIX.all.gff $FANOSUFFIX.all.$it.gff; cd -"
    log "$CMD41" CMD
    eval $CMD41

    # 4.2) Create HMM file
    log "4.2 Create HMM file"
    CMD42="cd $OUTDIR; $MAKERDIR/maker2zff $FANOSUFFIX.all.gff; $FATHOM -categorize 1000 genome.ann genome.dna; $FATHOM -export 1000 -plus uni.ann uni.dna; $FORGE export.ann export.dna; $HMMASSEMBLER $FANOSUFFIX.$it . > $FANOSUFFIX.$it.hmm; cd -"
    log "$CMD42" CMD
    eval $CMD42
  
    # 4.3) Change maker_opts.ctl
    log "4.3 Change maker_opts.ctl"
    CMD43="cat $OUTDIR/maker_opts.ctl.init | sed s:^est2genome=1:est2genome=0:g | sed s:^protein2genome=1:protein2genome=2:g | sed s:^snaphmm=:snaphmm=$FANOSUFFIX.$it.hmm:g > $OUTDIR/maker_opts.ctl; cp $OUTDIR/maker_opts.ctl $OUTDIR/maker_opts.ctl.$it"
    log "$CMD43" CMD
    eval $CMD43

    # 4.4) Rerun maker
    log "4.4 Rerun maker"
    CMD44="cd $OUTDIR; mpiexec -n $THREADS $MAKER; cd -"
    log "$CMD44" CMD
    eval $CMD44

done

# 5) Final grab of gff                                                                                                                                                                            
    FANOPATH=$(basename $GENOMEFA)
    FANOSUFFIX=${FANOPATH%.*}
    MAKERLOG=$OUTDIR/$FANOSUFFIX.maker.output/${FANOSUFFIX}_master_datastore_index.log
    CMD="cd $OUTDIR; $MAKERDIR/gff3_merge -d $MAKERLOG; cd -"
    log "$CMD" CMD
    eval $CMD

#Check for errors
SUCCESS=false
check_errors $TMP $SCRIPT $SUCCESS
if [ $SUCCESS = false ]; then
    exit
fi


#Finish up and output stats
ELAPSED=$(elapsed_since $START)
log "Success"
log "Module\tThreads\tTime(s)" STA
log "$(basename $0)\t${THREADS}\t${ELAPSED}" STA

#ERROR TAGS
#E# Example
