#!/usr/bin/env python
from optparse import OptionParser
import glob
import os
import para

#NB: Before use:
#chos 7



#Add to paths
addToPath=["/pub42/sam/Software/BRAKER1/augustus-3.2.3/bin/" , "/pub42/sam/Software/BRAKER1/augustus-3.2.3/scripts/"]
os.environ["PATH"]="%s:%s"%(':'.join(addToPath), os.environ["PATH"])
os.environ["AUGUSTUS_CONFIG_PATH"]="/pub42/sam/Software/BRAKER1/augustus-3.2.3/config/"


buscoDir = "/pub42/sam/Software/busco/"

def runBuscoPara(assemblies, outputDir, threads, databaseName):

    outputDir = os.path.abspath(outputDir)

    if not os.path.exists(outputDir):
        os.mkdir(outputDir)
    
    #Get files
    basenames = {} # { assembly path : basename } 
    fas = glob.glob("%s/*.fasta"%(os.path.abspath(opts.assemblies)))
    for fa in fas:
        basenames[fa] = os.path.basename(fa).replace(".fasta", "")

    #Prepare commands
    cmds = []

    for fa in basenames:
        bn = basenames[fa]
        cmdArr = ["cd %s"%buscoDir]
        cmdArr.append("./BUSCO.py -i %s -o %s -l %s -m geno --force -c 1"%(fa, bn, databaseName)) #Do not run with >1 threads: Will crash (bug in BLAST in BUSCO)
        cmdArr.append("mv %s/run_%s %s/%s"%(buscoDir, bn, outputDir, bn))
        cmd = ' ; '.join(cmdArr)
        cmds.append(cmd)
     
    #run commands
    para.runCommands(cmds, threads)

def consolidateBuscoResults(buscoDir, outputFn):
    o = open(outputFn, "w")
    o.write("Sample\tComplete\tDuplicate\tFragmented\tMissing\n")
    for resultFn in glob.glob("%s/*/short_summary*.txt"%(buscoDir)):
        name = os.path.basename(resultFn).replace("short_summary_","").replace(".txt","")
        line = [x for x in open(resultFn).readlines() if "C:" in x][0]
        spl = line.strip().split(":")
        cb = spl[1].split("%")[0] #complete
        db = spl[3].split("%")[0] #duplicate
        fb = spl[4].split("%")[0] #fragment
        mb = spl[5].split("%")[0] #missing
        o.write("%s\t%s\t%s\t%s\t%s\n"%(name, cb, db, fb, mb))
    o.close()
             


parser = OptionParser()
parser.add_option("-a", "--assemblies", help="directory containing assemblies in .fasta",  metavar="DIR")
parser.add_option("-o", "--output", help="output directory", metavar="DIR")
parser.add_option("-t", "--threads", help="threads [%default]", default=10, type=float)
parser.add_option("-d", "--database", help="database name [%default]", default="deltaepsilonsub_odb9")
(opts, args) = parser.parse_args()

#runBuscoPara(opts.assemblies, opts.output, opts.threads, opts.database)
consolidateBuscoResults(opts.output, opts.output+"/all.busco.csv")
