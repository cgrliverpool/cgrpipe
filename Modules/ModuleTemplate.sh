#!/bin/bash

#Get script path and directory
SCRIPT=$(readlink -e $0)
SCRIPTDIR=$(dirname $SCRIPT)
UTILS=$SCRIPTDIR/../Utils/

#Paths to software


#Source other files
source $UTILS/Logging
source $UTILS/Error

usage()
{
    cat <<EOF
    usage: $0 options

    OPTIONS:
     REQUIRED:
       -i <FILE>   Input 
       -o <FILE>   Output
    
     OPTIONAL:
       -h          Show this message
       -t <INT>    Threads [1]
       -e <FILE>   Log file [/dev/null]
EOF
}

#Create a timestamp
START=$(timestamp)

#Variables
IN=
OUT=
THREADS=1
LOGFILE=/dev/null

#Get command line options
while getopts “hi:o:t:e:” OPTION
do
    case $OPTION in

        #help
        h)      
            usage
            exit 1
            ;;
        #input
        i)
            IN=$OPTARG
            ;;	
	#output
	o)
	    OUT=$OPTARG
	    ;;
	#threads
	t)
	    THREADS=$OPTARG
	    ;;
	#log file
	e)
	    LOGFILE=$OPTARG
	    ;;
    esac
done

#Verify arguments
if [ -z  $IN ]; then 
    echo "Error: Must provide input file with -i"
    usage; exit
fi

if [ -z $OUT ]; then
    echo "Error: Must specify output file with -o"
    usage; exit
fi

#Capture stdout and stderr output to temp log file
TMP=$( mktemp -p ./ )
TMP=$(readlink -f $TMP)
exec >  >(tee -a $TMP >&1)
exec 2> >(tee -a $TMP >&2)

#Create full pathnames
IN=$(readlink -f $IN)
OUT=$(readlink -f $OUT)
LOGFILE=$(readlink -f $LOGFILE)

#Introduce script
log "*****"
log "Commencing $(basename $0)"
ICMD="$@"
log "Invoking command: $0 $ICMD" INV

#Command(s)

# 1) 
CMD=""
log "$CMD" CMD
eval $CMD

#Check for errors
SUCCESS=false
check_errors $TMP $SCRIPT $SUCCESS
if [ $SUCCESS = false ]; then
    exit
fi


#Finish up and output stats
ELAPSED=$(elapsed_since $START)
log "Success"
log "Module\tThreads\tTime(s)" STA
log "$(basename $0)\t${THREADS}\t${ELAPSED}" STA

#ERROR TAGS
#E# Example