#!/bin/bash

#Get script path and directory
SCRIPT=$(readlink -e $0)
SCRIPTDIR=$(dirname $SCRIPT)
UTILS=$SCRIPTDIR/../Utils/

#Paths to software


#Source other files
source $UTILS/Logging
source $UTILS/Error

usage()
{
    cat <<EOF
    usage: $0 options

    OPTIONS:
     REQUIRED:
       -i <FILE>   Input 
       -o <FILE>   Output
    
     OPTIONAL:
       -h          Show this message
       -t <INT>    Threads [1]
       -e <FILE>   Log file [/dev/null]
EOF
}

#Create a timestamp
START=$(timestamp)

#Variables
IN=
OUT=
THREADS=1
LOGFILE=/dev/null

#Get command line options
while getopts “hi:o:t:e:” OPTION
do
    case $OPTION in

        #help
        h)      
            usage
            exit 1
            ;;
        #input
        i)
            IN=$OPTARG
            ;;	
	#output
	o)
	    OUT=$OPTARG
	    ;;
	#threads
	t)
	    THREADS=$OPTARG
	    ;;
	#log file
	e)
	    LOGFILE=$OPTARG
	    ;;
    esac
done

#Verify arguments
if [ -z  $IN ]; then 
    echo "Error: Must provide input file with -i"
    usage; exit
fi

if [ -z $OUT ]; then
    echo "Error: Must specify output file with -o"
    usage; exit
fi

#Capture stdout and stderr output to temp log file
TMP=$( mktemp -p ./ )
TMP=$(readlink -f $TMP)
exec >  >(tee -a $TMP >&1)
exec 2> >(tee -a $TMP >&2)

#Create full pathnames
IN=$(readlink -f $IN)
OUT=$(readlink -f $OUT)
LOGFILE=$(readlink -f $LOGFILE)

#Introduce script
log "*****"
log "Commencing $(basename $0)"
ICMD="$@"
log "Invoking command: $0 $ICMD" INV

#Command(s)



###############################################################################
#Command(s)                                                                                                                                                                                                                                                                                                                        
VAR=${IN/.vcf/.varType.vcf}
NOLO=${VAR/.vcf/.noLo.vcf}

#Add variant types                                                                                                                                                                                                                                                                                                                 
java -jar /pub28/sam/bin/SnpSift.jar varType $IN > $VAR

#Remove LowQuals                                                                                                                                                                                                                                                                                                                   
awk '{if($7!="LowQual")print}' $VAR > $NOLO

INDEL=${NOLO/.vcf/.indel.vcf}
SNP=${NOLO/.vcf/.snp.vcf}

#Split into SNPs and INDELs                                                                                                                                                                                                                                                                                                        
cat $NOLO | java -jar /pub28/sam/bin/SnpSift.jar filter '(exists INS | exists DEL)' > $INDEL
cat $NOLO | java -jar /pub28/sam/bin/SnpSift.jar filter '(exists SNP)' > $SNP

#FILTER SNPs                                                                                                                                                                                                                                                                                                                       
SNPFILTERED=${SNP/.vcf/.filtered.vcf}

java -jar /pub28/sam/bin/GenomeAnalysisTK.jar -T VariantFiltration -R $REF -V $SNP --filterExpression "QD < 2.0" --filterName "QD" -o $SNP.1
java -jar /pub28/sam/bin/GenomeAnalysisTK.jar -T VariantFiltration -R $REF -V $SNP.1 --filterExpression "FS > 60.0 " --filterName "FS" -o $SNP.2
java -jar /pub28/sam/bin/GenomeAnalysisTK.jar -T VariantFiltration -R $REF -V $SNP.2 --filterExpression "MQ < 40.0 " --filterName "MQ" -o $SNP.3
java -jar /pub28/sam/bin/GenomeAnalysisTK.jar -T VariantFiltration -R $REF -V $SNP.3 --filterExpression "MQRankSum < -12.5" --filterName "MQRankSum" -o $SNP.4
java -jar /pub28/sam/bin/GenomeAnalysisTK.jar -T VariantFiltration -R $REF -V $SNP.4 --filterExpression "ReadPosRankSum < -8.0" --filterName "ReadPosRankSum" -o $SNPFILTERED # $SNP.5                                                                                                                                             
#Don't use this one for Exome sequencing                                                                                                                                                                                                                                                                                           
#java -jar /pub28/sam/bin/GenomeAnalysisTK.jar -T VariantFiltration -R $REF -V $SNP.5 --filterName "DP25" --filterExpression "DP < $DP25" --filterName "DP10" --filterExpression "DP < $DP10" -o $SNPFILTERED                                                                                                                      
rm $SNP.1 $SNP.2 $SNP.3 $SNP.4 $SNP.5
rm $SNP.1.idx $SNP.2.idx $SNP.3.idx $SNP.4.idx $SNP.5.idx
#FILTER INDELs                                                                                                                                                                                                                                                                                                                     
INDELFILTERED=${INDEL/.vcf/.filtered.vcf}

###############################################################################
# 1) 
CMD=""
log "$CMD" CMD
eval $CMD

#Check for errors
SUCCESS=false
check_errors $TMP $SCRIPT $SUCCESS
if [ $SUCCESS = false ]; then
    exit
fi


#Finish up and output stats
ELAPSED=$(elapsed_since $START)
log "Success"
log "Module\tThreads\tTime(s)" STA
log "$(basename $0)\t${THREADS}\t${ELAPSED}" STA

#ERROR TAGS
#E# Example