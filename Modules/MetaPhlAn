#!/bin/bash

#Get script path and directory
SCRIPT=$(readlink -e $0)
SCRIPTDIR=$(dirname $SCRIPT)
UTILS=$SCRIPTDIR/../Utils/

#Paths to software
METAPHLAN=/pub42/sam/bin/metaphlan2/metaphlan2.py
METAPHLAN_BT2_DB=/pub42/sam/bin/metaphlan2/db_v20/mpa_v20_m200

#Source other files
source $UTILS/Logging
source $UTILS/Error

usage()
{
    cat <<EOF
    usage: $0 options

    OPTIONS:
     REQUIRED:
       -i <FILE>   Input fq.gz's or fq's (can specify multiple, comma-sep. Must have same zip status)
       -o <FILE>   Output
    
     OPTIONAL:
       -h          Show this message
       -c          Remove previous metaphlan2 run [FALSE]
       -l <CHAR>   Taxonomic level (a=all,k,p,c,o,f,g or s) [a]
       -s <STRING> Sample name [Metaphlan2_Analysis]
       -t <INT>    Threads [1]
       -b <FILE>   Output BIOM table also
       -e <FILE>   Log file [/dev/null]

EOF
}

#Create a timestamp
START=$(timestamp)

#Variables
IN=
OUT=
THREADS=1
LEVEL=a
LOGFILE=/dev/null
CLEAN=false
SAMPLENAME=
BIOM=

#Get command line options
while getopts “hi:o:t:cl:s:b:e:” OPTION
do
    case $OPTION in

        #help
        h)      
            usage
            exit 1
            ;;
        #input
        i)
            IN=$OPTARG
            ;;	
	#output
	o)
	    OUT=$OPTARG
	    ;;
	#threads
	t)
	    THREADS=$OPTARG
	    ;;
	#clean?
	c)
	    CLEAN=true
	    ;;
	#Taxonomic level
	l)
	    LEVEL=$OPTARG
	    ;;
	#Sample name
	s)
	    SAMPLENAME="--sample_id $OPTARG"
	    ;;
	#Biom file
        b) 
	    BIOM="--biom $OPTARG"
	    ;;
        #log file
	e)
	    LOGFILE=$OPTARG
	    ;;
    esac
done

#Verify arguments
if [ -z  $IN ]; then 
    echo "Error: Must provide input file with -i"
    usage; exit
fi

if [ -z $OUT ]; then
    echo "Error: Must specify output file with -o"
    usage; exit
fi

#Check fq type
CATFUNC="cat"
suffix=$(echo "$IN" | rev | cut -d '.' -f1 | rev)
log "suffix is $suffix" INFO
if [[ "$suffix" == "gz" ]];
then
    CATFUNC="zcat"
fi

#Capture stdout and stderr output to temp log file
TMP=$( mktemp -p ./ )
exec >  >(tee -a $TMP >&1)
exec 2> >(tee -a $TMP >&2)

#Introduce script
log "*****"
log "Commencing $(basename $0)"
ICMD="$@"
log "Invoking command: $0 $ICMD" INV

#Command(s)

# -1) Split inputs by comma
IN=$(echo "$IN" | tr ',' ' ')

# 0) Remove existing metaphlan run
if [ "$CLEAN" = true ]; then
    CMD="rm $IN.bowtie2out.txt"
    log "$CMD" CMD
    eval $CMD
    
    #Check for errors
    SUCCESS=false
    check_errors $TMP $SCRIPT $SUCCESS
    if [ $SUCCESS = false ]; then
	exit
    fi
fi

# 1) Run MetaPhLan
CMD=". /pub42/sam/PythonEnv/bin/activate; $CATFUNC $IN | $METAPHLAN --mpa_pkl $METAPHLAN_BT2_DB.pkl --bowtie2db $METAPHLAN_BT2_DB --nproc $THREADS -o $OUT --input_type fastq --tax_lev $LEVEL $SAMPLENAME --bowtie2out $OUT.bt2 $BIOM"
log "$CMD" CMD
eval $CMD

#Check for errors
SUCCESS=false
check_errors $TMP $SCRIPT $SUCCESS
if [ $SUCCESS = false ]; then
    exit
fi


#Finish up and output stats
ELAPSED=$(elapsed_since $START)
log "Success"
log "Module\tThreads\tTime(s)" STA
log "$(basename $0)\t${THREADS}\t${ELAPSED}" STA

#ERROR TAGS
#E# BowTie2 output file detected
#E# No MetaPhlAn BowTie2 database found