#!/bin/bash
usage()
{
    cat <<EOF
    usage: $0 options

    OPTIONS:
       -h          Show this message
       -i <FILE>   Input file
       -r <FILE>   Reference file
EOF
}

#Variables
IN=

#Get command line options
while getopts “hi:r:” OPTION
do
    case $OPTION in

        #help
        h)      
            usage
            exit 1
            ;;

        #input
        i)
            IN=$OPTARG
            ;;
	#ref
	r)
	    REF=$OPTARG
	    ;;
   esac
done


#Command(s)
VAR=${IN/.vcf/.varType.vcf}
NOLO=${VAR/.vcf/.noLo.vcf}

#Add variant types
java -jar /pub28/sam/bin/SnpSift.jar varType $IN > $VAR

#Remove LowQuals
awk '{if($7!="LowQual")print}' $VAR > $NOLO

INDEL=${NOLO/.vcf/.indel.vcf}
SNP=${NOLO/.vcf/.snp.vcf}

#Split into SNPs and INDELs
cat $NOLO | java -jar /pub28/sam/bin/SnpSift.jar filter '(exists INS | exists DEL)' > $INDEL
cat $NOLO | java -jar /pub28/sam/bin/SnpSift.jar filter '(exists SNP)' > $SNP

#FILTER SNPs
SNPFILTERED=${SNP/.vcf/.filtered.vcf}

java -jar /pub28/sam/bin/GenomeAnalysisTK.jar -T VariantFiltration -R $REF -V $SNP --filterExpression "QD < 2.0" --filterName "QD" -o $SNP.1
java -jar /pub28/sam/bin/GenomeAnalysisTK.jar -T VariantFiltration -R $REF -V $SNP.1 --filterExpression "FS > 60.0 " --filterName "FS" -o $SNP.2
java -jar /pub28/sam/bin/GenomeAnalysisTK.jar -T VariantFiltration -R $REF -V $SNP.2 --filterExpression "MQ < 40.0 " --filterName "MQ" -o $SNP.3
java -jar /pub28/sam/bin/GenomeAnalysisTK.jar -T VariantFiltration -R $REF -V $SNP.3 --filterExpression "MQRankSum < -12.5" --filterName "MQRankSum" -o $SNP.4
java -jar /pub28/sam/bin/GenomeAnalysisTK.jar -T VariantFiltration -R $REF -V $SNP.4 --filterExpression "ReadPosRankSum < -8.0" --filterName "ReadPosRankSum" -o $SNPFILTERED # $SNP.5
#Don't use this one for Exome sequencing
#java -jar /pub28/sam/bin/GenomeAnalysisTK.jar -T VariantFiltration -R $REF -V $SNP.5 --filterName "DP25" --filterExpression "DP < $DP25" --filterName "DP10" --filterExpression "DP < $DP10" -o $SNPFILTERED
rm $SNP.1 $SNP.2 $SNP.3 $SNP.4 $SNP.5
rm $SNP.1.idx $SNP.2.idx $SNP.3.idx $SNP.4.idx $SNP.5.idx
#FILTER INDELs
INDELFILTERED=${INDEL/.vcf/.filtered.vcf}
java -jar /pub28/sam/bin/GenomeAnalysisTK.jar -T VariantFiltration -R $REF -V $INDEL --filterExpression "QD < 2.0" --filterName "QD" -o $INDEL.1
java -jar /pub28/sam/bin/GenomeAnalysisTK.jar -T VariantFiltration -R $REF -V $INDEL.1 --filterExpression "FS > 200.0" --filterName "FS" -o $INDEL.2
java -jar /pub28/sam/bin/GenomeAnalysisTK.jar -T VariantFiltration -R $REF -V $INDEL.2 --filterExpression "ReadPosRankSum < -20.0" --filterName "ReadPosRankSum" -o $INDELFILTERED #$INDEL.3
#java -jar /pub28/sam/bin/GenomeAnalysisTK.jar -T VariantFiltration -R $REF -V $INDEL.3 --filterName "DP25" --filterExpression "DP < $DP25" --filterName "DP10" --filterExpression "DP < $DP10" -o $INDELFILTERED 

rm $INDEL.1 $INDEL.2 $INDEL.3
rm $INDEL.1.idx $INDEL.2.idx $INDEL.3.idx

rm $NOLO $NOLO.idx
rm $INDEL $INDEL.idx
rm $SNP $SNP.idx
rm $VAR $VAR.idx

#Merge
MERGED=${IN/.vcf/.preCull.vcf}
java -jar /pub28/sam/bin/GenomeAnalysisTK.jar -T CombineVariants -R $REF --variant $SNPFILTERED --variant $INDELFILTERED -o $MERGED

rm $SNPFILTERED $SNPFILTERED.idx
rm $INDELFILTERED $INDELFILTERED.idx

FINAL=${IN/.vcf/.culled.vcf}
#cull low quality variants
cat <(cat $MERGED | grep '^##') <(echo "##Cull=Deleted SnpFilter|IndelFilter|LowQual") <(cat $MERGED | grep '^#C') <(cat $MERGED | grep -v '^#' | awk '{if (index($7,"QD")==0 && index($7,"FS")==0 && index($7,"ReadPosRankSum")==0 && index($7,"MQ")==0 && index($7,"LowQual")==0) print}' ) > $FINAL