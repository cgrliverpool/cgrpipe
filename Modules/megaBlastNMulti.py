#!/usr/bin/env python
from optparse import OptionParser
import glob
import os
import para

def runMegablastPara(assemblies, outputDir, threads, procs, databaseName):

    if not os.path.exists(outputDir):
        os.mkdir(outputDir)
    
    #Get files
    basenames = {} # { assembly path : basename } 
    fas = glob.glob("%s/*.fasta"%(os.path.abspath(opts.assemblies)))
    for fa in fas:
        basenames[fa] = os.path.basename(fa).replace(".fasta", "")

    #Prepare commands
    cmds = []

    for fa in basenames:
        bn = basenames[fa]
        oname = "%s/%s.blast"%(outputDir, bn)
        cmd="blastn -task megablast -query %s -db %s -num_threads %s -outfmt '6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qlen slen' > %s"%(fa, databaseName, threads, oname )
        cmds.append(cmd)
     
    #run commands
    para.runCommands(cmds, procs)


parser = OptionParser()
parser.add_option("-a", "--assemblies", help="directory containing assemblies in .fasta",  metavar="DIR")
parser.add_option("-o", "--output", help="output directory", metavar="DIR")
parser.add_option("-t", "--threads", help="threads for BLAST [%default]", default=32, type=int)
parser.add_option("-p", "--procs", help="concurrent BLAST runs [%default]", default=10, type=int)
parser.add_option("-d", "--database", help="database name [%default]", default="/pub28/sam/Databases/Bacterial/prokaryotes_and_viruses_Apr2016.fa")
(opts, args) = parser.parse_args()

runMegablastPara(opts.assemblies, opts.output, opts.threads, opts.procs, opts.database)

