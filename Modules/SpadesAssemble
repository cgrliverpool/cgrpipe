#!/bin/bash

#Get script path and directory
SCRIPT=$(readlink -e $0)
SCRIPTDIR=$(dirname $SCRIPT)
UTILS=$SCRIPTDIR/../Utils/

#Path to software
#SPADES=/pub28/sam/bin/spades.py
SPADES=/pub28/sam/Software/SPAdes-3.7.0-Linux/bin/spades.py
#Source other files
source $UTILS/Logging
source $UTILS/Error

usage()
{
    cat <<EOF
    usage: $0 options

    OPTIONS:
     REQUIRED:
       -a <FILE>   R1 FASTQ (can be .gz)
       -b <FILE>   R2 FASTQ (can be .gz)
       -o <DIR>    Output directory
       -k <STRING> Kmer sizes, comma-sep (e.g. 25,65)
       -x          Metagone assembly [false]

     OPTIONAL:
       -h          Show this message
       -s          Single-end FASTQ (can be .gz)
       -c          Correct reads before assembly [FALSE]
       -t          Threads [1]
       -m <INT>    Max memory GB [50]
       -e <FILE>   Log file [/dev/null]

EOF
}

#Create a timestamp
START=$(timestamp)

#Variables
FQ1=
FQ2=
META=""
OUTDIR=
KMER="25,65"
SE=
THREADS=1
MAXMEM=50
LOGFILE=/dev/null
NOCORRECT="--only-assembler"

#Get command line options
while getopts “ha:b:o:k:s:ct:xm:e:” OPTION
do
    case $OPTION in

        #help
        h)      
            usage
            exit 1
            ;;
        #FQ1
        a)
            FQ1=$OPTARG
            ;;	
	#FQ2
	b)
	    FQ2=$OPTARG
	    ;;

	#Output file prefix
	o)
	    OUTDIR=$OPTARG
	    ;;
	#Metagenome
        x)
	    META=" --meta "
	    ;;
        #Kmer sizes
	k)
	    KMER=$OPTARG
	    ;;
	#SE Reads
	s)
	    SE="-s $OPTARG"
	    ;;
	#Threads
	t)
	    THREADS=$OPTARG
	    ;;
	#Correct reads
	c)
	    NOCORRECT=""
	    ;;
	#Max Memory
	m)
	    MAXMEM=$OPTARG
	    ;;
	#log file
	e)
	    LOGFILE=$OPTARG
	    ;;
    esac
done

#Verify arguments
if [[ -z  $FQ1 || -z $FQ2 ]]; then 
    echo "Error: Must provide input files with -a and -b"
    usage; exit
fi

if [ -z $OUTDIR ]; then
    echo "Error: Must specify output directory with -o"
    usage; exit
fi

#if [ -z $KMER ]; then
#    echo "Error: Must specify Kmer sizes with -k"
#    usage; exit
#fi


#Capture stdout and stderr output to temp log file
TMP=$( mktemp -p ./ )
exec >  >(tee -a $TMP >&1)
exec 2> >(tee -a $TMP >&2)

#Introduce script
log "*****"
log "Commencing $(basename $0)"
ICMD="$@"
log "Invoking command: $0 $ICMD" INV

#Command(s)

# 1) 
CMD="$SPADES -1 $FQ1 -2 $FQ2 $SE -k $KMER -t $THREADS -m $MAXMEM -o $OUTDIR $NOCORRECT $META"
echo $CMD
log "$CMD" CMD
eval $CMD

#Check for errors
SUCCESS=false
check_errors $TMP $SCRIPT $SUCCESS
if [ $SUCCESS = false ]; then
    exit
fi

#Finish up and output stats
ELAPSED=$(elapsed_since $START)
log "Success"
log "Module\tThreads\tTime(s)" STA
log "$(basename $0)\t${THREADS}\t${ELAPSED}" STA

#ERROR TAGS
#E# Verification of expression 'cov_.size() > 10'
#E# == Error ==
