#!/bin/bash

#Get script path and directory
SCRIPT=$(readlink -e $0)
SCRIPTDIR=$(dirname $SCRIPT)
UTILS=$SCRIPTDIR/../Utils/

#Paths to software


#Source other files
source $UTILS/Logging
source $UTILS/Error

usage()
{
    cat <<EOF
    usage: $0 options

    OPTIONS:
     REQUIRED:
       -b <FILE>   RNASeq Bam File
       -o <FILE>   Output Dir
       -g <FILE>   Genome FASTA
       -s <STRING> Species name
       -f          Fungal genome       
    
     OPTIONAL:
       -h          Show this message
       -t <INT>    Threads [1]
       -e <FILE>   Log file [/dev/null]
EOF
}

#Create a timestamp
START=$(timestamp)

#Variables
BAM=
OUT=
GENOME=
THREADS=1
SPECIES=""
FUNGAL=""
LOGFILE=/dev/null

#Get command line options
while getopts “ha:b:o:g:t:e:s:f” OPTION
do
    case $OPTION in

        #help
        h)      
            usage
            exit 1
            ;;
	#BAM
	b)
	    BAM=$OPTARG
	    ;;
	#genome
	g)
	    GENOME=$OPTARG
	    ;;
	#output
	o)
	    OUT=$OPTARG
	    ;;
	#species
	s)
	    SPECIES=" --species $OPTARG "
	    ;;
	#fungal?
	f)
	    FUNGAL=" --fungus "
	    ;;
	#threads
	t)
	    THREADS=$OPTARG
	    ;;
	#log file
	e)
	    LOGFILE=$OPTARG
	    ;;
    esac
done

#Verify arguments
#if [ -z  $R1S ]; then 
#    echo "Error: Must provide input file with -a and -b"
#    usage; exit
#fi

if [ -z $GENOME ]; then
    echo "Error: Must specify genome file with -g"
    usage; exit
fi

if [ -z $OUT ]; then
    echo "Error: Must specify output file with -o"
    usage; exit
fi

#Capture stdout and stderr output to temp log file
TMP=$( mktemp -p ./ )
TMP=$(readlink -f $TMP)
exec >  >(tee -a $TMP >&1)
exec 2> >(tee -a $TMP >&2)

#Create full pathnames
OUT=$(readlink -f $OUT)
LOGFILE=$(readlink -f $LOGFILE)
GENOME=$(readlink -f $GENOME)
#Introduce script
log "*****"
log "Commencing $(basename $0)"
ICMD="$@"
log "Invoking command: $0 $ICMD" INV

echo "$OUT"
#Command(s)
cmd="PATH=/pub42/sam/Software/BRAKER1/:/pub42/sam/Software/BRAKER1/augustus-3.2.3/bin:/pub42/sam/Software/BRAKER1/augustus-3.2.3/scripts/:$PATH; AUGUSTUS_CONFIG_PATH=/pub42/sam/Software/BRAKER1/augustus-3.2.3/config/ ; /pub42/sam/Software/BRAKER1/braker.pl  --GENEMARK_PATH /pub42/sam/Software/gm_et_linux_64/gmes_petap/ --BAMTOOLS_PATH /pub42/sam/Software/BRAKER1/bamtools/bin/ --bam $BAM --genome $GENOME $SPECIES $FUNGAL --cores $THREADS --workingdir $OUT" 
log "$cmd" CMD
echo "$cmd"

chos 7 bash <( 
cat <<EOF
 eval $cmd
 exit
 cat
EOF
)


#chos 7 bash <(
#
#cat <<EOF
#
#eval $cmd
#
##Revert to OS5
#exit
#cat
#EOF
#)


