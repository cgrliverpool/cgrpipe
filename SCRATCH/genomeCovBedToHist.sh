#!/bin/bash

cat $1 | awk 'BEGIN{max=0;}{if($4>max)max=$4; tot[$4]+=$3-$2}END{for (i=0;i<=max;++i){if (tot[i]<1)tot[i]=0;print i"\t"tot[i]}}'